local tween = require "tween"
local art = require "components.art"
local tabletop = require "utils.tabletop"

-- Sets the default state
local art_controller = {
    animations = {
        image = { alpha = 0, state = "changing" },
        text = { alpha = 0, state = "showing" },
    },
}

-- Creates all the tweens
function art_controller:load()
    -- Image tweens
    self.animations.image.transition = tween.new(0.5, self.animations.image, {alpha = 1}, 'inQuad')

    -- Text tweens
    self.animations.text.fadeIn = tween.new(0.5, self.animations.text, {alpha = 1}, 'inQuad')
    self.animations.text.fadeOut = tween.new(0.5, self.animations.text, {alpha = 0}, 'inQuad')
end

-- Resets all animations
function art_controller:changeScreen()
    -- Reset all animations
    self.animations.image.transition:reset()
    self.animations.image.state = "changing"

    self.animations.text.fadeOut:reset()
    self.animations.text.state = "hiding"
end

-- Updates all effects
function art_controller:update(dt)
    -- Update image animation
    art_controller:updateImageTween(dt)
    
    local current_screen = tabletop.data.screens[tabletop.data.screens.current]
    local last_screen = tabletop.data.screens[tabletop.data.screens.current - 1]

    -- Update text animation
    art_controller:updateTextTween(dt, current_screen, last_screen)
end

function art_controller:updateImageTween(dt)
    if self.animations.image.state == "changing" then
        local complete = self.animations.image.transition:update(dt)
        if complete then self.animations.image.state = "waiting" end
    end
end

function art_controller:updateTextTween(dt, current_screen, last_screen)
    if not current_screen.map and self.animations.text.state == "showing" then
        local complete = self.animations.text.fadeIn:update(dt)
        if complete then self.animations.text.state = "waiting" end
    end
    
    if last_screen and not last_screen.map and self.animations.text.state == "hiding" then
        local complete = self.animations.text.fadeOut:update(dt)

        if complete then
            self.animations.text.fadeIn:reset()
            self.animations.text.state = "showing"
        end
    end
end

-- Draws the art
function art_controller:draw(resources)
    local current_screen = tabletop.data.screens[tabletop.data.screens.current]
    local last_screen = tabletop.data.screens[tabletop.data.screens.current - 1]

    -- Draws the last image
    if last_screen and not last_screen.map and self.animations.image.state == "changing" then
        love.graphics.setColor(1, 1, 1, 1 - self.animations.image.alpha)
        art.draw(resources.oldImage, 0, 0)
    end

    -- Draws the current image
    if not current_screen.map then
        love.graphics.setColor(1, 1, 1, self.animations.image.alpha)

        local dx = 0
        if tabletop.data.screens.current > 1 then
            dx = love.graphics.getWidth() * (1 - self.animations.image.alpha)
        end
        
        art.draw(resources.image, dx, 0)
    end

    -- Draw the text
    art_controller:drawText(last_screen, current_screen)
end

function art_controller:drawText(last_screen, current_screen)
    local _screen_width, _screen_height = love.graphics.getDimensions()

    local text = nil
    if last_screen and not last_screen.map and self.animations.text.state == "hiding" then
        text = last_screen.title
    elseif not current_screen.map then
        text = current_screen.title
    end

    if text then
        love.graphics.setColor(0, 0, 0, self.animations.text.alpha * 0.5)
        love.graphics.rectangle("fill", 0, _screen_height - 16 - 24, _screen_width, 24)
        love.graphics.setColor(1, 1, 1, self.animations.text.alpha)
        love.graphics.printf(text, 0, _screen_height - 16 - 18, _screen_width, "center")
    end
end

return art_controller