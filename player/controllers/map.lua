local tween = require "tween"
local map = require "components.map"
local chip = require "components.chip"
local tabletop = require "utils.tabletop"
local pathway = require "utils.pathway"

-- Sets the default state
local map_controller = {
    camera = { row = 0, column = 0, oldRow = 0, oldColumn = 0 },
    animations = {
        dt = 0,
        image = { alpha = 0, state = "changing" },
        text = { alpha = 0, dt = 0, state = "showing" },
        chip = { value = 0, state = "hide" }
    }
}

-- Creates all the tweens
function map_controller:load()
    -- Image tweens
    self.animations.image.transition = tween.new(0.5, self.animations.image, {alpha = 1}, 'inQuad')
    -- Text tweens
    self.animations.text.transition = tween.new(0.5, self.animations.text, {alpha = 1}, 'inQuad')
    -- Chip tweens
    self.animations.chip.transition = tween.new(0.5, self.animations.chip, {value = 1}, 'inExpo')
end

-- Updates all effects
function map_controller:update(dt, resources)
    local current_map = tabletop.data.screens[tabletop.data.screens.current]
    local last_map = tabletop.data.screens[tabletop.data.screens.current - 1]

    -- Checks waiting
    if last_map and not last_map.map then
        if self.animations.dt < 1 then
            self.animations.dt = math.min(self.animations.dt + dt, 1)
        end
    end

    if not last_map or last_map.map or self.animations.dt == 1 then
        -- Updates image effect
        map_controller:updateImageTween(dt)
        -- Updates text effect
        map_controller:updateTextTween(dt)
        -- Updates chip effect
        map_controller:updateChipTween(dt)
    end
end

function map_controller:updateImageTween(dt)
    if self.animations.image.state == "changing" then
        local complete = self.animations.image.transition:update(dt)
        if complete then self.animations.image.state = "waiting" end
    end
end

function map_controller:updateTextTween(dt)
    if self.animations.text.state == "showing" or self.animations.text.state == "hiding" then
        local complete = self.animations.text.transition:update(dt)
        if complete then 
            if self.animations.text.state == "showing" then
                self.animations.text.state = "waiting"
            else
                self.animations.text.state = "hide"
            end
        end
    end
    
    if self.animations.text.state == "waiting" and self.animations.text.dt < 2 then
        self.animations.text.dt = math.min(self.animations.text.dt + dt, 2)
        if self.animations.text.dt == 2 then
            self.animations.text.transition:reset()
            self.animations.text.state = "hiding"
        end
    end
end

function map_controller:updateChipTween(dt)
    if self.animations.chip.state == "showing" or self.animations.chip.state == "hiding" then
        local complete = self.animations.chip.transition:update(dt)
        if complete then
            if self.animations.chip.state == "showing" then
                self.animations.chip.state = "waiting"
            else
                self.animations.chip.state = "hide"
            end
        end
    end
end

-- Reset all efects
function map_controller:changeScreen()
    self.animations.image.transition:reset()
    self.animations.text.transition:reset()
    self.animations.dt = 0
    self.animations.text.dt = 0
    self.animations.image.state = "changing"
    self.animations.text.state = "showing"

    self.camera.oldRow = self.camera.row
    self.camera.oldColumn = self.camera.column

    local current_map = tabletop.data.screens[tabletop.data.screens.current]

    if current_map.map then
        self.camera.row = current_map.start[1]
        self.camera.column = current_map.start[2]
    end
end

-- Change camera point
function map_controller:pointAt(row, column)
    self.camera.row = row
    self.camera.column = column
end

-- Set a character chip to show in the middle of the screen
function map_controller:showChip(character)
    self.animations.chip.character = character
    self.animations.chip.transition:reset()
    self.animations.chip.state = "showing"
end

-- Hides the current character chip
function map_controller:hideChip()
    self.animations.chip.transition:reset()
    self.animations.chip.state = "hiding"
end

-- Checks if we are showing a chip
function map_controller:showingChip()
    return self.animations.chip.state ~= "hide"
end

-- Draws the map
function map_controller:draw(resources)
    local current_map = tabletop.data.screens[tabletop.data.screens.current]
    local last_map = tabletop.data.screens[tabletop.data.screens.current - 1]

    local _screen_width, _screen_height = love.graphics.getDimensions()

    -- Draws the last map
    if last_map and last_map.map and self.animations.image.state == "changing" then
        love.graphics.setColor(1, 1, 1, 1 - self.animations.image.alpha)
        map.draw(resources.oldImage, _screen_width, _screen_height, self.camera.oldRow, self.camera.oldColumn)
    end

    if current_map.map then
        -- Draws the current map
        love.graphics.setColor(1, 1, 1, self.animations.image.alpha)
        local map_x, map_y = map.draw(resources.image, _screen_width, _screen_height, self.camera.row, self.camera.column)

        if _DEBUG then
            -- Draw the walls
            for i, column in ipairs(current_map.walls) do
                for j, wall in ipairs(column) do
                    if wall then
                        love.graphics.setColor(1, 0, 0, 0.2 * self.animations.image.alpha)
                        love.graphics.rectangle("fill", map_x + (j - 1) * 70, map_y + (i - 1) * 70, 70, 70)
                    end
                end
            end

            -- Draw the fog
            for i, column in ipairs(current_map.fog) do
                for j, fog in ipairs(column) do
                    if fog then
                        love.graphics.setColor(0, 0, 0, 0.2 * self.animations.image.alpha)
                        love.graphics.rectangle("fill", map_x + (j - 1) * 70, map_y + (i - 1) * 70, 70, 70)
                    end
                end
            end
        end

        -- Draws the characters
        map_controller:drawCharacters(map_x, map_y, current_map, "players", tabletop.data.players, resources)
        map_controller:drawCharacters(map_x, map_y, current_map, "npcs", current_map.npcs, resources)

        -- Draw the fog
        if not _DEBUG then
            for i, column in ipairs(current_map.fog) do
                for j, fog in ipairs(column) do
                    if fog then
                        love.graphics.setColor(0, 0, 0)
                        love.graphics.rectangle("fill", map_x + (j - 1) * 70, map_y + (i - 1) * 70, 70, 70)
                    end
                end
            end
        end

        -- Draw the text
        if self.animations.text.state ~= "hide" then
            map_controller:drawText(current_map, _screen_width, _screen_height)
        end

        -- Draw the chip
        if self.animations.chip.state ~= "hide" then
            map_controller:drawChip(self.animations.chip.character, _screen_width, _screen_height)
        end
    end
end

-- Draw a list of characters
function map_controller:drawCharacters(map_x, map_y, current_map, type, characters, resources)
    for name, character in pairs(characters) do
        -- Gets the state of the character
        local state = tabletop.state.characters[name]

        if not state.hidden then
            -- Gets the resources
            local portrait = resources[type][name]
            local frame = resources.portraits[character.portrait]

            -- Calculates the position
            local cx = map_x + state.position[1] * 70
            local cy = map_y + state.position[2] * 70

            -- Draws the path way
            if state.movement then
                local path = pathway.find({ row = state.position[1] + 1, column = state.position[2] + 1 }, math.floor(character.speed / 1.5), current_map.walls, {rows = current_map.size[1], columns = current_map.size[2]})

                for _, cell in ipairs(path) do
                    if _DEBUG then love.graphics.setColor(0, 1, 0, 0.5 * self.animations.image.alpha)
                    else love.graphics.setColor(0, 0.5, 1, 0.5 * self.animations.image.alpha) end
                    love.graphics.rectangle("fill", map_x + (cell.column - 1) * 70, map_y + (cell.row - 1) * 70, 70, 70)

                    if _DEBUG then
                        love.graphics.setColor(1, 1, 1, self.animations.image.alpha)
                        love.graphics.print(cell.step, map_x + (cell.column - 1) * 70, map_y + (cell.row - 1) * 70)
                        love.graphics.setColor(0, 0, 0, self.animations.image.alpha)
                        love.graphics.printf(cell.i, map_x + (cell.column - 1) * 70, map_y + (cell.row - 1) * 70, 70, "right")
                    end
                end
            end

            -- Draws the chip
            chip.setFrameColor(character.color)
            chip.setOpacity(self.animations.image.alpha)
            chip.draw({ portrait = portrait, frame = frame }, cx, cy, state.alive, state.poisoned, state.paralyzed, state.afire)
        end
    end
end

function map_controller:drawText(current_map, _screen_width, _screen_height)
    local alpha = self.animations.text.alpha
    if self.animations.text.state == "hiding" then alpha = 1 - alpha end

    love.graphics.setColor(0, 0, 0, alpha * 0.5)
    love.graphics.rectangle("fill", 0, _screen_height / 2 - 12, _screen_width, 24)
    love.graphics.setColor(1, 1, 1, alpha)

    local font = love.graphics.getFont()
    local width = font:getWidth(current_map.title)

    local x = _screen_width / (alpha * 2) - width / (alpha * 2)
    if self.animations.text.state == "hiding" then
        x = _screen_width / 2 - width / 2
    end
    love.graphics.setColor(1, 1, 1, alpha)
    love.graphics.print(current_map.title, x, _screen_height / 2 - 6)
end

function map_controller:drawChip(character, _screen_width, _screen_height)
    local value = self.animations.chip.value
    if self.animations.chip.state == "hiding" then value = 1 - value end

    -- Draws the background
    love.graphics.setColor(0, 0, 0, value * 0.5)
    love.graphics.rectangle("fill", 0, 0, _screen_width, _screen_height)

    -- Calculates the portrait size
    local _portrait_size = 226 * value

    -- Calculates the portrait ratio
    local _portrait_rx = _portrait_size / character.portrait:getWidth()
    local _portrait_ry = _portrait_size / character.portrait:getHeight()
    local _portrait_ratio = math.max(_portrait_rx, _portrait_ry)

    -- Calculates the portrait position
    local _portrait_dx = math.floor(_screen_width / 2 - _portrait_size / 2)
    local _portrait_dy = math.floor(_screen_height / 2 - _portrait_size / 2)

    -- Draws the portrait
    love.graphics.setColor(1, 1, 1)
    love.graphics.stencil(function()
        love.graphics.circle("fill", math.floor(_screen_width / 2), math.floor(_screen_height / 2), _portrait_size / 2)
    end)
    love.graphics.setStencilTest("greater", 0)
    love.graphics.setColor(1, 1, 1, value)
    love.graphics.draw(character.portrait, _portrait_dx, _portrait_dy, 0, _portrait_ratio, _portrait_ratio)
    love.graphics.setStencilTest()

    -- Calculates the frame size
    local _frame_size = 256 * value

    -- Calculates the frame ratio aspect
    local _frame_rx = _frame_size / character.frame:getWidth()
    local _frame_ry = _frame_size / character.frame:getHeight()
    local _frame_ratio = math.max(_frame_rx, _frame_ry)
    
    -- Calculates the frame position
    local _frame_dx = math.floor(_screen_width / 2 - _frame_size / 2)
    local _frame_dy = math.floor(_screen_height / 2 - _frame_size / 2)

    -- Draws the frame
    local alpha = (character.frameColor[4] or 1) * value
    love.graphics.setColor(character.frameColor[1], character.frameColor[2], character.frameColor[3], alpha)
    love.graphics.draw(character.frame, _frame_dx, _frame_dy, 0, _frame_ratio, _frame_ratio)
end

return map_controller