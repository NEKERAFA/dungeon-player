local map = {}

function map.draw(image, _screen_width, _screen_height, row, column)
    local _image_width, _image_height = image:getDimensions()

    -- Calculates the ratio aspect
    local rx = _screen_width / _image_width
    local ry = _screen_height / _image_height

    -- Calculates the position
    local x = 0
    if rx > 1 then
        x = math.floor(_screen_width / 2 - _image_width / 2)
    else
        x = math.max(math.min(0, _screen_width / 2 - row * 70), - _image_width + _screen_width)
    end

    local y = 0
    if ry > 1 then
        y = math.floor(_screen_height / 2 - _image_height / 2)
    else
        y = math.max(math.min(0, _screen_height / 2 - column * 70), - _image_height + _screen_height)
    end

    -- Draws the image
    love.graphics.draw(image, x, y)

    return x, y
end

return map