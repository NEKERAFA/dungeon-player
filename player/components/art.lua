local art = {}

function art.draw(image, x, y)
    local _screen_width, _screen_height = love.graphics.getDimensions()
    local _image_width, _image_height = image:getDimensions()

    -- Calculates the ratio aspect
    local rx = _screen_width / _image_width
    local ry = _screen_height / _image_height
    local ratio = math.min(rx, ry)
    
    -- Calculates the size
    local _image_new_width = math.floor(_image_width * ratio)
    local _image_new_height = math.floor(_image_height * ratio)

    -- Calculates the position
    local _art_x = x + math.floor(_screen_width / 2 - _image_new_width / 2)
    local _art_y = y + math.floor(_screen_height / 2 - _image_new_height / 2)

    -- Draw the image
    love.graphics.draw(image, _art_x, _art_y, 0, ratio, ratio)
end

return art