local chip = {}

local _chip_frameColor = {1, 1, 1}
local _chip_opacity = 1

function chipStencil(cx, cy)
    return function() love.graphics.circle("fill", cx, cy, 33, 33) end
end

function chip.setFrameColor(color)
    _chip_frameColor = color
end

function chip.setOpacity(opacity)
    _chip_opacity = opacity
end

function chip.draw(character, x, y, alive, poisoned, paralyzed, afire)
    -- Calculates the portrait ratio aspect
    local _portrait_rx = 66 / character.portrait:getWidth()
    local _portrait_ry = 66 / character.portrait:getHeight()
    local _portrait_ratio = math.max(_portrait_rx, _portrait_ry)
    
    -- Calculates the portrait position
    local _portrait_dx = math.floor(35 - character.portrait:getWidth() * _portrait_ratio / 2)
    local _portrait_dy = math.floor(35 - character.portrait:getHeight() * _portrait_ratio / 2)

    -- Draws the portrait
    love.graphics.stencil(chipStencil(x + 35, y + 35))
    love.graphics.setStencilTest("greater", 0)

    if alive then love.graphics.setColor(1, 1, 1, _chip_opacity)
    else love.graphics.setColor(0.5, 0.5, 0.5, _chip_opacity) end

    love.graphics.draw(character.portrait, x + _portrait_dx, y + _portrait_dy, 0, _portrait_ratio, _portrait_ratio)
    love.graphics.setStencilTest()

    -- Calculates the frame ratio aspect
    local _frame_rx = 70 / character.frame:getWidth()
    local _frame_ry = 70 / character.frame:getHeight()
    local _frame_ratio = math.max(_frame_rx, _frame_ry)
    
    -- Calculates the frame position
    local _frame_dx = math.floor(35 - character.frame:getWidth() * _frame_ratio / 2)
    local _frame_dy = math.floor(35 - character.frame:getHeight() * _frame_ratio / 2)

    -- Draws the frame
    local alpha = (_chip_frameColor[4] or 1) * _chip_opacity
    love.graphics.setColor(_chip_frameColor[1], _chip_frameColor[2], _chip_frameColor[3], alpha)
    love.graphics.draw(character.frame, x + _frame_dx, y + _frame_dy, 0, _frame_ratio, _frame_ratio)

    -- Draws the alterations
    local dx = 0
    
    if poisoned then
        love.graphics.setColor(0.5, 0, 1, _chip_opacity)
        love.graphics.circle("fill", x + 65, y + 5, 5)
        dx = 10
    end

    if paralyzed then
        love.graphics.setColor(1, 1, 0.25, _chip_opacity)
        love.graphics.circle("fill", x + 65 - dx, y + 5, 5)
        dx = dx + 10
    end

    if afire then
        love.graphics.setColor(1, 0.25, 0, _chip_opacity)
        love.graphics.circle("fill", x + 65 - dx, y + 5, 5)
    end
end

return chip