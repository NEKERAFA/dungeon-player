local tween = require "tween"
local tabletop = require "utils.tabletop"
local art = require "controllers.art"
local map = require "controllers.map"

local resources = {
    image = nil, oldImage = nil, music = nil, oldMusic = nil,
    players = nil, npcs = nil
}

local music = { volume = 0, state = "changing" }

if _DEBUG then

    -- Create a new map and save it
    function create_map(path)
        tabletop.new()

        tabletop.data.screens = {
            {
                title = "República de Grayardia",
                assets = {
                    image = "Republic of Grayardia.png",
                    music = "intro.wav"
                },
                map = false
            },
            {
                title = "- Ciénaga de Swanforest -",
                assets = {
                    image = "Swanforest.png",
                    music = "campaign.ogg"
                },
                size = { 20, 20 },
                start = { 16, 16 },
                walls = {
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, true, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false, false, false, false, false, true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false, false, false, false, false, true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, true,  true,  true,  true,  true,  false, true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false, false, false, true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false, false, false, false, true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false, false, false, false, true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                },
                fog = {
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, true, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {false, false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true, true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false, false, false, true,  true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false, false, false, false, true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false, false, false, false, true,  true},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  false, false, false, false, false},
                    {true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true},
                },
                npcs = {
                    ["Goblin 1"] = {
                        image = "Goblin-Female.png",
                        portrait = 1,
                        color = { 1, 1, 1 },
                        speed = 9
                    }
                },
                map = true
            },
            {
                title = "Muelles de Grayard",
                assets = {
                    image = "Grayard-Artwork.png",
                },
                map = false
            },
            current = 1,
            count = 3
        }

        tabletop.data.players = {
            ["Lavinia"] = {
                image = "Fighter-Female2.jpg",
                portrait = 3,
                color = { 0.5, 0, 1 },
                speed = 6
            }
        }

        tabletop.state.screen = 2
        tabletop.state.characters = {
            ["Goblin 1"] = {
                position = { 12, 8 },
                alive = true,
                poisoned = true,
                paralyzed = true,
                afire = true,
                hidden = false,
                movement = false
            },
            ["Lavinia"] = {
                position = { 16, 16 },
                alive = true,
                poisoned = false,
                paralyzed = false,
                afire = false,
                hidden = false,
                movement = true
            }
        }

        tabletop.saveState(path)
    end

end

function love.load(arg)

    if _DEBUG then
        -- Creates a map and save it
        create_map(arg[1])
    end

    -- Loads the controllers
    art:load()
    map:load()

    -- Loads the tweens
    loadMusicTween()

    -- Loads the tabletop file
    tabletop.load(arg[1])
    tabletop.loadState(arg[2])

    love.window.setTitle("Dungeon Player - " .. tabletop.data.name)

    -- Loads the assets
    loadPlayersAssets()
    loadScreenAssets()
    loadPortraits()
end

-- Creates the tweens for music
function loadMusicTween()
    music.transition = tween.new(1, music, {volume = 1}, 'inQuad')
end

-- Loads the assets of the players
function loadPlayersAssets()
    resources.players = {}

    for name, player in pairs(tabletop.data.players) do
        resources.players[name] = tabletop.getAsset("image", "players/" .. player.image)
    end
end

-- Loads the portraits
function loadPortraits()
    resources.portraits = {}

    for i = 1, 3 do
        resources.portraits[i] = love.graphics.newImage("assets/images/portrait-" .. i .. ".png")
    end
end

-- Loads the assets of the current scene if there isn't loaded
function loadScreenAssets()
    local current_screen = tabletop.data.screens[tabletop.data.screens.current]

    -- Removes old image if exists
    if resources.oldImage then resources.oldImage:release() end

    -- Set the current image as old
    if resources.image then resources.oldImage = resources.image end

    -- Loads the image
    local folder = "art"
    if current_screen.map then folder = "maps" end
    resources.image = tabletop.getAsset("image", folder .. "/" .. current_screen.assets.image)
    
    -- Loads a music if exists
    if current_screen.assets.music then
        -- Removes old music if exists
        if resources.oldMusic then resources.oldMusic:release(); resources.oldMusic = nil end

        -- Sets the current music as old
        if resources.music ~= nil then resources.oldMusic = resources.music end

        -- Loads the music
        resources.music = tabletop.getAsset("music", "bgm/" .. current_screen.assets.music)
        resources.music:play()
        resources.music:setLooping(true)
        resources.music:setVolume(0)
    end

    -- Loads the npc if the screen is a map
    if current_screen.map then
        if not resources.npcs then resources.npcs = {} end

        for name, npc in pairs(current_screen.npcs) do
            resources.npcs[name] = tabletop.getAsset("image", "npcs/" .. npc.image)
        end
    end
end

-- Changes the current screen
function changeScreen()
    if tabletop.data.screens.current < tabletop.data.screens.count then
        -- Change screen
        tabletop.data.screens.current = tabletop.data.screens.current + 1
        loadScreenAssets()

        local current_screen = tabletop.data.screens[tabletop.data.screens.current]
        if current_screen.assets.music then
            -- Reset all music animation
            music.transition:reset()
            music.state = "changing"
        end

        -- Call the controllers that we will change screen
        map:changeScreen()
        art:changeScreen()
    end
end

function love.update(dt)
    -- Updates all controllers
    map:update(dt)
    art:update(dt)

    updateTweens(dt)
end

-- Updates all music effects
function updateTweens(dt)
    if resources.music and music.state == "changing" then
        local complete = music.transition:update(dt)
        resources.music:setVolume(music.volume)

        if resources.oldMusic then
            resources.oldMusic:setVolume(1 - music.volume)
        end

        if complete then music.state = "waiting" end
    end
end

if _DEBUG then

    function love.keypressed(key, scancode)
        if scancode == "return" then
            if map:showingChip() then
                map:hideChip()
            else
                changeScreen()
            end
        end

        if tabletop.data.screens[tabletop.data.screens.current].map then
            if scancode == "up" and map.camera.column > 0 then
                map.camera.column = map.camera.column - 1
            elseif scancode == "down" and map.camera.column < resources.image:getHeight() / 70 - 1 then
                map.camera.column = map.camera.column + 1
            end

            if scancode == "left" and map.camera.row > 0 then
                map.camera.row = map.camera.row - 1
            elseif scancode == "right" and map.camera.row < resources.image:getWidth() / 70 - 1 then
                map.camera.row = map.camera.row + 1
            end

            if scancode == "1" then
                map:pointAt(12, 8)
            elseif scancode == "2" then
                map:pointAt(16, 16)
            end

            if scancode == "k" then
                tabletop.data.players["Lavinia"].killed = not tabletop.data.players["Lavinia"].killed
            end

            if scancode == "s" and not map:showingChip() then
                local portrait = resources.players["Lavinia"]
                local frame = resources.portraits[tabletop.data.players["Lavinia"].portrait]
                local frameColor = tabletop.data.players["Lavinia"].color
                map:showChip({ portrait = portrait, frame = frame, frameColor = frameColor })
            end
        end
    end

end

function love.draw()
    -- Draws all controllers
    map:draw(resources)
    art:draw(resources)

    if _DEBUG then
        if tabletop.data.screens[tabletop.data.screens.current].map then
            local screenWidth, screenHeight = love.window.getMode()
            local current_screen = tabletop.data.screens[tabletop.data.screens.current]

            -- Calculates the ratio aspect
            local rx = screenWidth / resources.image:getWidth()
            local ry = screenHeight / resources.image:getHeight()

            -- Calculates the position
            local x, y = 0, 0
            if rx > 1 then
                x = math.floor(screenWidth / 2 - resources.image:getWidth() / 2)
            else
                x = math.max(math.min(0, screenWidth / 2 - map.camera.row * 70), - resources.image:getWidth() + screenWidth)
            end

            if ry > 1 then
                y = math.floor(screenHeight / 2 - resources.image:getHeight() / 2)
            else
                y = math.max(math.min(0, screenHeight / 2 -map.camera.column * 70), - resources.image:getHeight() + screenHeight)
            end

            love.graphics.setColor(1, 0, 0)
            love.graphics.rectangle("line", x + map.camera.row * 70, y + map.camera.column * 70, 70, 70)

            love.graphics.setColor(1, 1, 1)
            love.graphics.print(string.format("camera: %d, %d", map.camera.row, map.camera.column), 10, 24)
        end

        love.graphics.setColor(1, 1, 1)
        love.graphics.print(string.format("image: %.2f\ttext: %.2f\tmusic: %.2f\tmap: %.2f\ttitle: %.2f (%s)\tchip: %.2f (%s)", art.animations.image.alpha, art.animations.text.alpha, music.volume, map.animations.image.alpha, map.animations.text.alpha, map.animations.text.state, map.animations.chip.value, map.animations.chip.state), 10, 10)
    end

end