local locale = {
    welcome = "Add images to create a new role table",
    new = "New",
    open = "Open",
    save = "Save",
    play = "Start game",
    walls = "Walls",
    fog = "Fog",
    wallpaper = "Set as wallpaper"
}

return { ["en-GB"] = locale }