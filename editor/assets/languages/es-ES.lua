local locale = {
    welcome = "Añada imágenes para crear un tablero de rol",
    new = "Nuevo",
    open = "Abrir",
    save = "Guardar",
    play = "Empezar partida",
    walls = "Muros",
    fog = "Niebla",
    wallpaper = "Establecer como imagen fija"
}

return { ["es-ES"] = locale }