local locale = {
    welcome = "Engada imaxes para crear un taboeiro de rol",
    new = "Novo",
    open = "Abrir",
    save = "Gardar",
    play = "Empezar partida",
    walls = "Muros",
    fog = "Néboa",
    wallpaper = "Establecer como imaxe fixa"
}

return { ["gl-ES"] = locale }