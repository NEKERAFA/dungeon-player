local toolbar = require "components.toolbar"
local navegation = require "components.navegation"
local nfd = require "nfd"

local editor = {}

function editor:load()
    -- Loads toolbar
    toolbar:load()
    toolbar.icons[2].onclicked = editor.onSaveFile
    toolbar.icons[3].onclicked = editor.onOpenFile

    -- Loads navegation
    navegation:load()
end

function editor.onOpenFile()
    nfd.open("dungeon", nil)
end

function editor.onSaveFile()
    nfd.save("dungeon", nil)
end

function editor:mousemoved(mx, my, button)
    toolbar:mousemoved(mx, my, button)
    navegation:mousemoved(mx, my, button)
end

function editor:mousepressed(mx, my, button)
    toolbar:mousepressed(mx, my, button)
    navegation:mousepressed(mx, my, button)
end

function editor:draw()
    navegation:draw()
    toolbar:draw()
end

return editor