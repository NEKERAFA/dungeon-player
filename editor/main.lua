local material = require "libs.material"

local editor = require "controllers.editor"

local state = editor

function love.load()
    love.graphics.setBackgroundColor(material.theme.getBackground())
    love.graphics.setColor(material.theme.getOnBackground())
    love.graphics.setFont(material.texts.getBody("sans"))

    if state.load then state:load() end
end

function love.mousemoved(x, y, dx, dy, istouch)
    if state.mousemoved then state:mousemoved(x, y, dx, dy, istouch) end
end

function love.mousepressed(x, y, button, istouch, presses)
    if state.mousepressed then state:mousepressed(x, y, button, istouch, presses) end
end

function love.update(dt)
    if state.update then state:update(dt) end
end

function love.draw()
    if state.draw then state:draw() end
end