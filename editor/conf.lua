_APP_VERSION = "Dungeon Editor 0.0.1"

_DEBUG = true

local _VERSION_NUMBER = string.match(_VERSION, "(%d+%.%d+)")

package.cpath = 'libs\\lua\\' .. _VERSION_NUMBER .. '\\?.dll;' .. package.cpath
package.path = 'libs\\?.lua;libs\\?\\init.lua;' .. package.path

function love.conf(t)
    t.identity = "DungeonEditor"
    t.version = "11.3"
    t.console = _DEBUG

    t.window.title = "Dungeon Editor - tabletop.dungeon*"
    t.window.icon = "assets/images/icon.png"
    t.window.width = 640
    t.window.height = 480
    t.window.highdpi = true
    t.window.resizable = true
end
