local material = require "libs.material"
local mlib = require "mlib"

local navegation = {}

function navegation:load()
    local _ib_size = material.buttons.getIbPixelSize()
    local _screen_height = love.graphics.getHeight()

    -- Loads the icons
    self.icons = {
        {
            name = "grid_on",
            x = 2, y = 2 + _ib_size,
            inactive = false, hover = false, checked = false, clicked = false
        },
        {
            name = "cloud",
            x = 2, y = 2 + _ib_size * 2,
            inactive = false, hover = false, checked = false, clicked = false
        }
    }
end

function navegation:mousemoved(mx, my)
    local radius = material.buttons.getIbPixelSize() / 2

    for i, icon in ipairs(self.icons) do
        icon.hover = mlib.circle.checkPoint(mx, my, icon.x + radius, icon.y + radius, radius)
    end
end

function navegation:mousepressed(mx, my, button)
    for i, icon in ipairs(self.icons) do
        icon.clicked = icon.hover
        if icon.clicked then
            icon.checked = not icon.checked
            if icon.onclicked then icon.onclicked() end
        end
    end
end

function navegation:draw()
    local _screen_width, _screen_height = love.graphics.getDimensions()
    local _ib_size = material.buttons.getIbPixelSize()
    local _line_style = love.graphics.getLineStyle()

    -- Draws the background
    love.graphics.setColor(material.theme.getSurface())
    love.graphics.rectangle("fill", 0, _ib_size, _ib_size, _screen_height - _ib_size)

    local _default_color = material.theme.getOnSurface()

    -- Draws the icon
    for i, icon in ipairs(self.icons) do
        if icon.checked then
            material.theme.setOnSurface(material.theme.palette["blue"]["500"])
        end
        material.buttons.iconButton(icon.name, icon.x, icon.y, nil, icon.inactive, icon.hover, icon.clicked)
        material.theme.setOnSurface(_default_color)
    end

    -- Draws the border
    love.graphics.setColor(0, 0, 0, 0.5)
    love.graphics.setLineStyle("rough")
    love.graphics.line(_ib_size, _ib_size, _ib_size, _screen_height)

    love.graphics.setLineStyle(_line_style)
end

return navegation