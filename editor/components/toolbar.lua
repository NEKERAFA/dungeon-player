local material = require "libs.material"
local mlib = require "mlib"

local toolbar = {}

function toolbar:load()
    local _ib_size = material.buttons.getIbPixelSize()
    local _screen_width = love.graphics.getWidth()

    -- Loads the icons
    self.icons = {
        {
            name = "note_add",
            x = 2, y = 2,
            inactive = false, hover = false, clicked = false
        },
        {
            name = "save",
            x = 2 + _ib_size, y = 2,
            inactive = false, hover = false, clicked = false
        },
        {
            name = "edit",
            x = 2 + _ib_size * 2, y = 2,
            inactive = false, hover = false, clicked = false
        },
        {
            name = "play_arrow",
            x = 2 + _ib_size * 3, y = 2,
            inactive = false, hover = false, clicked = false
        }
    }

    -- Loads the shadow
    self.shadow = material.shadow(_screen_width, _ib_size, 8, function(w, h)
        love.graphics.setColor(0, 0, 0, 1)
        love.graphics.rectangle("fill", math.floor(w / 2 - _screen_width / 2), math.floor(h / 2 - _ib_size / 2, _screen_width), _screen_width, _ib_size)
    end)
end

function toolbar:mousemoved(mx, my)
    local radius = material.buttons.getIbPixelSize() / 2

    for i, icon in ipairs(self.icons) do
        icon.hover = mlib.circle.checkPoint(mx, my, icon.x + radius, icon.y + radius, radius)
    end
end

function toolbar:mousepressed(mx, my, button)
    for i, icon in ipairs(self.icons) do
        icon.clicked = icon.hover
        if icon.clicked and icon.onclicked then icon.onclicked() end
    end
end

function toolbar:draw()
    local _screen_width, _screen_height = love.graphics.getDimensions()
    local _shadow_width, _shadow_height = self.shadow:getDimensions()
    local _ib_size = material.buttons.getIbPixelSize()

    -- Draws the shadow
    love.graphics.setColor(0, 0, 0, 0.2)
    love.graphics.draw(self.shadow, math.floor(_screen_width / 2 - _shadow_width / 2), math.floor(_ib_size / 2 - _shadow_height / 2))

    -- Draws the background
    love.graphics.setColor(material.theme.getSurface())
    love.graphics.rectangle("fill", 0, 0, _screen_width, _ib_size)

    -- Draws the icon
    for i, icon in ipairs(self.icons) do
        material.buttons.iconButton(icon.name, icon.x, icon.y, nil, icon.inactive, icon.hover, icon.clicked)
    end
end

return toolbar