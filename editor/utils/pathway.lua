-- Based on: https://gamedev.stackexchange.com/a/167719

local pathway = {}

-- Populates a dictionary of reachable tiles.
function pathway.find(start, steps, walls, size)
    local reachable = {}

    -- Checks if start point is reachable.
    if not walls[start.row][start.column] then
        -- Seed the pathfinding with our start point.
        table.insert(reachable, { row = start.row, column = start.column, step = 0, i = 0 })
        local set = {
            {row = start.row, column = start.column, step = 0},
            count = 1
        }

        -- As long as there are nodes in the frontier...
        local i = 1
        while set.count > 0 do
            -- Pop the oldest one out of the collection.
            local parent = table.remove(set, 1)
            set.count = set.count - 1

            -- Check each neighbouring cell for reachability,
            -- and add it to the collection if we can reach it.
            local childStep = parent.step + 1
            for row = -1, 1 do
                for column = -1, 1 do
                    if math.abs(row) + math.abs(column) == 1 then
                        local child = { row = parent.row + row, column = parent.column + column, i = i }
                        
                        if pathway.checkChild(child, childStep, steps, walls, size, reachable) then
                            table.insert(set, { row = child.row, column = child.column, step = childStep })
                            set.count = set.count + 1
                            i = i + 1
                        end
                    end
                end
            end
        end
    end

    return reachable
end

function pathway.checkChild(node, nodeStep, steps, walls, size, reachable)
    -- Exclude nodes that are out of bounds or not walkable
    if pathway.checkOutOfBound(node, size) or walls[node.row][node.column] then
        return false
    end

    -- Exclude nodes we've already visited by some other route.
    if pathway.checkContains(node, reachable) then
        return false
    end

    -- Aha! We found a new walkable node in range. Add it to our reachable set.
    table.insert(reachable, { row = node.row, column = node.column, step = nodeStep, i = node.i })
    
    -- And if we have room to go further, add it to our frontier
    -- to later explore its neighbouring nodes.
    return nodeStep < steps
end

function pathway.checkContains(node, reachable)
    for _, position in ipairs(reachable) do
        if position.row == node.row and position.column == node.column then
            return true
        end
    end

    return false
end

function pathway.checkOutOfBound(node, size)
    return node.row < 1 and node.row > size.rows and node.column < 1 and node.column > size.columns
end

return pathway