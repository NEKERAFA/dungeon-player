local bitser = require 'bitser'

local tabletop = {}

local saveDirectory = string.gsub(love.filesystem.getSaveDirectory(), "/", "\\")

local function recursiveRemove(item)
    if love.filesystem.getInfo(item, "directory") then
        for _, child in pairs(love.filesystem.getDirectoryItems(item)) do
            recursiveRemove(item .. "/" .. child)
            love.filesystem.remove(item .. "/" .. child)
        end
        love.filesystem.remove(item)
    elseif love.filesystem.getInfo(item) then
        love.filesystem.remove(item)
    end
end

function tabletop.load(filename)
    -- Copy file to temp file
    local tempFile = saveDirectory .. "\\temp.7z"
    if os.execute("copy " .. filename .. " " .. tempFile .. " >NUL") ~= 0 then
        error("cannot copy " .. filename)
    end

    -- Extract tabletop files
    local tabletopDirectory = saveDirectory .. "\\tabletop"
    if os.execute(".\\bin\\7za.exe x -y -bsp0 -bso0 -o" .. tabletopDirectory .. " " .. tempFile) ~= 0 then
        error("cannot open " .. filename)
    end

    -- Remove temp file
    os.remove(tempFile)

    -- Load data
    local data = bitser.loadLoveFile("tabletop/dungeon.dat")

    if data.version then
        local _MAYOR, _MINOR, _REV = string.match(_APP_VERSION, "(%d+)%.(%d+)%.(%d+)")
        local _DATA_MAYOR, _DATA_MINOR, _DATA_REV = string.match(data.version, "(%d+)%.(%d+)%.(%d+)")

        if _MAYOR ~= _DATA_MAYOR or _MINOR ~= _DATA_MINOR or _REV ~= _DATA_REV then
            -- Conversion chains

            error("version not supported")
        end
    end

    tabletop.data = data
end

function tabletop.loadState(state)
    local filename = "state_" .. tonumber(state) .. ".sav"
    if not love.filesystem.getInfo("tabletop/" .. filename) then
        error("cannot open " .. filename)
    end

    -- Load state
    local state = bitser.loadLoveFile("tabletop/" .. filename)

    if state.version then
        local _MAYOR, _MINOR, _REV = string.match(_APP_VERSION, "(%d+)%.(%d+)%.(%d+)")
        local _DATA_MAYOR, _DATA_MINOR, _DATA_REV = string.match(state.version, "(%d+)%.(%d+)%.(%d+)")

        if _MAYOR ~= _DATA_MAYOR or _MINOR ~= _DATA_MINOR or _REV ~= _DATA_REV then
            -- Conversion chains

            error("version not supported")
        end
    end

    tabletop.state = state
end

function tabletop.new()
    tabletop.data = {
        version = _APP_VERSION,
        name = "Tabletop",
        screens = {current = 0, count = 0},
        players = {}
    }

    tabletop.state = {
        version = _APP_VERSION,
        state = 1,
        characters = {},
        screen = 0
    }

    -- Clear temporal directory
    --recursiveRemove("tabletop")

    -- Save dungeon and state
    love.filesystem.createDirectory("tabletop")
    bitser.dumpLoveFile("tabletop/dungeon.dat", tabletop.data)
    bitser.dumpLoveFile("tabletop/state_1.sav", tabletop.data)
end

function tabletop.getAsset(type, filename)
    if love.filesystem.getInfo("tabletop/" .. filename) then
        if type == "image" then
            return love.graphics.newImage("tabletop/" .. filename)
        elseif type == "music" then
            return love.audio.newSource("tabletop/" .. filename, "stream")
        end
    end

    return nil
end

function tabletop.save(path)
    bitser.dumpLoveFile("tabletop/dungeon.dat", tabletop.data)

    local filename = string.match(path, "(.+)%.%w*")
    
    -- Create zip archive
    local tabletopDirectory = saveDirectory .. "\\tabletop"
    if os.execute(".\\bin\\7za.exe a -y -bsp0 -bso0 -t7z " .. filename .. ".7z " .. tabletopDirectory .. "\\*") ~= 0 then
        error("cannot save " .. filename .. ".dungeon")
    end

    -- Checks if file name exists
    os.remove(filename .. ".dungeon")

    -- Rename extension
    ok, msg = os.rename(filename .. ".7z", filename .. ".dungeon")
    if ok == nil then error(msg) end
end

function tabletop.saveState(path)
    bitser.dumpLoveFile("tabletop/dungeon.dat", tabletop.data)
    bitser.dumpLoveFile("tabletop/state_" .. tonumber(tabletop.state.state) .. ".sav", tabletop.state)

    local filename = string.match(path, "(.+)%.%w*")
    
    -- Create zip archive
    local tabletopDirectory = saveDirectory .. "\\tabletop"
    if os.execute(".\\bin\\7za.exe a -y -bsp0 -bso0 -t7z " .. filename .. ".7z " .. tabletopDirectory .. "\\*") ~= 0 then
        error("cannot save " .. filename .. ".dungeon")
    end

    -- Checks if file name exists
    os.remove(filename .. ".dungeon")

    -- Rename extension
    ok, msg = os.rename(filename .. ".7z", filename .. ".dungeon")
    if ok == nil then error(msg) end
end

function tabletop.clear()
    recursiveRemove("tabletop")
end

return tabletop